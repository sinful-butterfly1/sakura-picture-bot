import discord
from discord.ext import commands
import urllib.request
import json
from bs4 import BeautifulSoup
import requests
import os
import random
from discord.ext.commands import has_permissions, CheckFailure


bot = commands.Bot(command_prefix='>')


def get_picture():
    res = requests.get("https://danbooru.donmai.us/posts?ms=1&page=1&tags=matou_sakura+rating%3Asafe")
    html = res.text
    soup = BeautifulSoup(html, 'html.parser')
    pages_div = soup.findAll('li', attrs={'class': 'numbered-page'})
    pages = pages_div[-1].text
    page_to_visit = random.randint(1, int(pages))
    req = urllib.request.Request("https://danbooru.donmai.us/posts.json?ms=1&page=" + str(page_to_visit) + "&tags=matou_sakura+rating%3Asafe", headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' })
    r = urllib.request.urlopen(req).read()
    data = json.loads(r.decode('utf-8'))
    pic = None
    try:
        random_index = random.randint(0, len(data))
        pic = data[random_index]["file_url"]
    except KeyError:
        pic = data[0]["file_url"]
    
    return pic
    

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    game = discord.Game("with Senpai! | >sakura")
    await bot.change_presence(status=discord.Status.do_not_disturb, activity=game)
    
@bot.command(pass_context=True, description="Sends a random picture of Matou Sakura")
async def sakura(ctx):
    res = get_picture()
    embed = discord.Embed(
        title = "Here is your picture, senpai!",
        colour = discord.Colour.purple()

    )
    embed.set_image(url=res)
    #embed.set_thumbnail(url=bot.user.avatar_url)
    embed.set_author(name="Bot by chernoburkv#0001", icon_url="https://cdn.discordapp.com/avatars/613083072728006687/6ef847251a047b4b3937dc7a7ebb4af1.webp?size=512")
    await ctx.send(embed=embed)
@bot.command(pass_context=True, description="Delete a specified amount of messages. If no amount is supplied the bot will only delete the 10 most recent ones")
@has_permissions(administrator=True, manage_messages=True, manage_roles=True)
async def clear(ctx, amount=10):
    channel = ctx.message.channel
    async for message in channel.history(limit=int(amount)):
        await message.delete()
    
        

@clear.error
async def clear_error(error, ctx):
    if isinstance(error, CheckFailure):
        await ctx.send(ctx.message.channel, "Looks like you don't have the perms")

bot.run("TOKEN HERE")
